import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExclusionGroupComponent } from './exclusion-group.component';

describe('ExclusionGroupComponent', () => {
  let component: ExclusionGroupComponent;
  let fixture: ComponentFixture<ExclusionGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExclusionGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExclusionGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
