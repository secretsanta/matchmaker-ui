import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ExclusionGroup} from './exclusion-group';
import {Person} from '../person/person';

@Component({
  selector: 'app-exclusion-group',
  templateUrl: './exclusion-group.component.html',
  styleUrls: ['./exclusion-group.component.scss']
})
export class ExclusionGroupComponent implements OnInit {

  @Input() exclusionGroup: ExclusionGroup;
  @Input() people: Array<Person>;
  @Output() exclusionRemoved = new EventEmitter<number>();
  indexToAdd: number;

  constructor() { }

  ngOnInit() {
  }

  removeExclusion(personIndex: number) {
    this.exclusionRemoved.emit(personIndex);
  }

  addExclusion(personIndex: number) {
    if (personIndex != null) {
      this.exclusionGroup.peopleIndexes.push(personIndex);
    }
  }
}
