import { Component } from '@angular/core';
import {ReplaySubject} from 'rxjs';
import {SecretSantaParty} from './secret-santa-party';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'matchmaker-ui';
  selectedParty = new ReplaySubject<SecretSantaParty>();
  updateParties = new ReplaySubject<null>();
}
