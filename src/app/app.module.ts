import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PersonComponent } from './person/person.component';
import { ExclusionGroupComponent } from './exclusion-group/exclusion-group.component';
import { PartyListComponent } from './party-list/party-list.component';
import { HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { PartyComponent } from './party/party.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonComponent,
    ExclusionGroupComponent,
    PartyListComponent,
    PartyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
