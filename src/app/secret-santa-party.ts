import {Person} from './person/person';
import {ExclusionGroup} from './exclusion-group/exclusion-group';

export class SecretSantaParty {
  people: Array<Person> = [];
  exclusionGroups: Array<ExclusionGroup> = [];
  id: string;
  name: string;
}
