import { TestBed } from '@angular/core/testing';

import { SecretSantaService } from './secret-santa.service';

describe('SecretSantaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SecretSantaService = TestBed.get(SecretSantaService);
    expect(service).toBeTruthy();
  });
});
