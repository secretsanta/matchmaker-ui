import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SecretSantaParty} from './secret-santa-party';
import {environment} from '../environments/environment';
import {GiftExchange} from './gift-exchange';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SecretSantaService {

  constructor(
    private http: HttpClient
  ) { }

  getParties(): Observable<Array<SecretSantaParty>> {
    return this.http.get<SecretSantaParty[]>(environment.serviceUrl + '/api/party');
  }

  saveParty(party: SecretSantaParty) {
    return this.http.post<SecretSantaParty>(environment.serviceUrl + '/api/party', party);
  }

  updateParty(party: SecretSantaParty) {
    return this.http.put<SecretSantaParty>(environment.serviceUrl + '/api/party', party);
  }

  reset() {
    return this.http.delete<null>(environment.serviceUrl + '/purge');
  }


  getGiftExchanges(selectedParty: SecretSantaParty) {
    return this.http.get<Array<GiftExchange>>(environment.serviceUrl + '/api/party/' + selectedParty.id + '/matches');
  }
}
