import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SecretSantaService} from '../secret-santa.service';
import {SecretSantaParty} from '../secret-santa-party';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-party-list',
  templateUrl: './party-list.component.html',
  styleUrls: ['./party-list.component.scss']
})
export class PartyListComponent implements OnInit {

  @Input() updateParties: Observable<null>;
  @Output() selectedPartyChanged = new EventEmitter<SecretSantaParty>();

  selectedParty: SecretSantaParty = null;

  parties: Array<SecretSantaParty>;

  constructor(private secretSantaService: SecretSantaService) {
  }

  ngOnInit() {
    this.getParties();

    this.updateParties.subscribe(() => this.getParties());
  }

  getParties() {
    this.secretSantaService.getParties()
      .subscribe(parties => {
        this.parties = parties;
      });
  }

  resetPartyList() {
    this.secretSantaService.reset().subscribe(() => this.getParties());
  }

  createParty() {
    const newParty = new SecretSantaParty();
    this.parties.push(newParty);
    this.selectedParty = newParty;
    this.selectedPartyChanged.emit(newParty);
  }


  selected(party: SecretSantaParty) {
    this.selectedPartyChanged.next(party);
    this.selectedParty = party;
  }
}
