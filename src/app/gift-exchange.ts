import {Person} from './person/person';

export class GiftExchange {
  giver: Person;
  recipient: Person;
}
