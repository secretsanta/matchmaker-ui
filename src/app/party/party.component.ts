import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Person} from '../person/person';
import {SecretSantaParty} from '../secret-santa-party';
import {SecretSantaService} from '../secret-santa.service';
import {Observable} from 'rxjs';
import {flatMap, tap} from 'rxjs/operators';
import {ExclusionGroup} from '../exclusion-group/exclusion-group';
import {GiftExchange} from '../gift-exchange';

@Component({
  selector: 'app-party',
  templateUrl: './party.component.html',
  styleUrls: ['./party.component.scss']
})
export class PartyComponent implements OnInit {

  @Input() selectedPartyChanged: Observable<SecretSantaParty>;
  @Output() updateParties = new EventEmitter<null>();

  selectedParty: SecretSantaParty;
  noMatchExists: boolean;
  giftExchanges: Array<GiftExchange>;

  constructor(private secretSantaService: SecretSantaService ) { }

  ngOnInit() {
    this.selectedPartyChanged.subscribe((party) => {
      this.selectedParty = party;
      this.clearOutput();
    });
  }

  addPerson() {
    this.selectedParty.people.push(new Person());
  }

  removePerson(i: number) {
    this.clearOutput();

    // Because the exclusion groups use indexes, we have to take care of them first. The index we're removing is filtered out of all
    // of the exclusion groups, and then we decrement any index in the exclusion groups above the one we removed
    for (const exclusionGroup of this.selectedParty.exclusionGroups) {
      exclusionGroup.peopleIndexes = exclusionGroup.peopleIndexes
        .filter(exclusionIndex => exclusionIndex !== i);

      exclusionGroup.peopleIndexes = exclusionGroup.peopleIndexes
        .map(exclusionIndex => (exclusionIndex >= i) ? exclusionIndex - 1 : exclusionIndex);
    }

    // Prune exclusion groups that only have one person
    this.cleanExclusionGroups();

    // Actually removeExcludedPerson the person we want to removeExcludedPerson
    this.selectedParty.people.splice(i, 1);
  }


  saveParty() {
    let obs: Observable<SecretSantaParty>;

    if (this.selectedParty.id != null) {
      obs = this.secretSantaService.updateParty(this.selectedParty);
    } else {
      obs = this.secretSantaService.saveParty(this.selectedParty);
    }

    // Save without changing parties
    return obs.pipe(
      tap(party => this.selectedParty = party), // do this so we have an ID to query for in the case of getMatches
      tap(() => this.updateParties.emit())
    );
  }

  getMatches() {
    this.saveParty().pipe(
      flatMap(() => this.secretSantaService.getGiftExchanges(this.selectedParty))
    )
      .subscribe(
        giftExchanges => this.giftExchanges = giftExchanges,
        () => {
          this.clearOutput();
          this.noMatchExists = true;
        }
      );
  }

  clearOutput() {
    this.giftExchanges = null;
    this.noMatchExists = false;
  }

  removeExcludedPerson(i: number, personIndex: number) {
    this.clearOutput();

    this.selectedParty.exclusionGroups[i].peopleIndexes = this.selectedParty.exclusionGroups[i].peopleIndexes.filter(j => personIndex !== j);

    this.cleanExclusionGroups();
  }

  private cleanExclusionGroups() {
    this.selectedParty.exclusionGroups = this.selectedParty.exclusionGroups
      .filter(exclusionGroup => exclusionGroup.peopleIndexes.length > 1);
  }

  addExclusionGroup() {
    this.selectedParty.exclusionGroups.push(new ExclusionGroup());
  }

}
