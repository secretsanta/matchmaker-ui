# MatchmakerUi

This is the UI that goes with the [MatchMaker server project](https://gitlab.com/secretsanta/matchmaker). Run the program using the below instructions or alternatively run the container from the built-in registry. If you are using the container, expose port 80.

Running the UI in prod mode will cause it to use the publicly visible service at https://secretsanta-matchmaker.secretsanta.olbrich.io/api/party. You can modify the environments URL to point to your own instance.

The UI can be visited at http://home.olbrich.io:8080/

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
